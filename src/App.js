import logo from './logo.svg';
import './App.css';
import Content from './views/contents';

function App() {
  document.title = 'Little Alchemy'

  return (
    <div className="App" id='element'>
        <div className='main'>
          <Content/>
        </div>
    </div>
  );
}

export default App;
