let filterList = [];
for (let i = 97; i <= 122; i++) {
  const data = {
    id: i,
    name: String.fromCharCode(i).toUpperCase(),
  };
  filterList.push(data);
}

export default filterList