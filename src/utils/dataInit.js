import air from "../assets/images/air.png";
import earth from "../assets/images/earth.png";
import fire from "../assets/images/fire.png";
import water from "../assets/images/water.png";
import energy from "../assets/images/energy.png";
import rain from "../assets/images/rain.png";
import mud from "../assets/images/mud.png";
import plant from "../assets/images/plant.png";
import swamp from "../assets/images/swamp.png";
import life from "../assets/images/life.png";
import bacterial from "../assets/images/bacterial.png";
import bird from "../assets/images/bird.png";
import egg from "../assets/images/egg.png";
import omelette from "../assets/images/omelette.png";
import hardroe from "../assets/images/hardroe.png";
import algae from "../assets/images/algae.png";
import duck from "../assets/images/duck.png";
import duckling from "../assets/images/duckling.png";
import dust from "../assets/images/dust.png";
import flood from "../assets/images/flood.png";
import grass from "../assets/images/grass.png";
import garden from "../assets/images/garden.png";
import gunpowder from "../assets/images/gunpowder.png";
import human from "../assets/images/human.png";
import love from "../assets/images/love.png";
import obsidian from "../assets/images/obsidian.png";
import lava from "../assets/images/lava.png";
import ostrich from "../assets/images/ostrich.png";
import phoenix from "../assets/images/phoenix.png";
import plankton from "../assets/images/plankton.png";
import pond from "../assets/images/pond.png";
import pressure from "../assets/images/pressure.png";
import rose from "../assets/images/rose.png";
import sea from "../assets/images/sea.png";
import seagull from "../assets/images/seagull.png";
import seaweek from "../assets/images/seaweek.png";
import angel from "../assets/images/angel.png";
import allergy from "../assets/images/allergy.png";
import dew from "../assets/images/dew.png";
import cold from "../assets/images/cold.png";
import sickness from "../assets/images/sickness.png";
import ocean from "../assets/images/ocean.png";
import perfume from "../assets/images/perfume.png";
import steam from "../assets/images/steam.png";
import stone from "../assets/images/stone.png";
import tobacco from "../assets/images/tobacco.png";
import volcano from "../assets/images/volcano.png";
import cloud from "../assets/images/cloud.png";
import sky from "../assets/images/sky.png";
import moon from "../assets/images/moon.png";
import astronaut from "../assets/images/astronaut.png";
import ice from "../assets/images/ice.png";
import sand from "../assets/images/sand.png";
import glass from "../assets/images/glass.png";
import time from "../assets/images/time.png";
import hourglass from "../assets/images/hourglass.png";
import tree from "../assets/images/tree.png";
import forest from "../assets/images/forest.png";
import wildAnimal from "../assets/images/wild animal.png";
import dog from "../assets/images/dog.png";
import livestock from "../assets/images/livestock.png";
import cow from "../assets/images/cow.png";
import milk from "../assets/images/milk.png";
import minotaur from "../assets/images/minotaur.png";
import iceCream from "../assets/images/ice cream.png";
import astronautIceCream from "../assets/images/astronaut ice cream.png";
import cat from "../assets/images/cat.png";
import wind from "../assets/images/wind.png";
import wave from "../assets/images/wave.png";
import sound from "../assets/images/sound.png";
import music from "../assets/images/music.png";
import keyboardCat from "../assets/images/keyboard cat.png";
import metal from "../assets/images/metal.png";
import tool from "../assets/images/tool.png";
import wood from "../assets/images/wood.png";
import charcoal from "../assets/images/charcoal.png";
import smoke from "../assets/images/smoke.png";
import campfire from "../assets/images/campfire.png";
import lake from "../assets/images/lake.png";
import story from "../assets/images/story.png";
import nessie from "../assets/images/nessie.png";
import night from "../assets/images/night.png";
import blade from "../assets/images/blade.png";
import star from "../assets/images/star.png";
import shuriken from "../assets/images/shuriken.png";
import ninja from "../assets/images/ninja.png";
import turtle from "../assets/images/turtle.png";
import ninjaTurtle from "../assets/images/ninja turtle.png";
import coal from "../assets/images/coal.png";
import diamond from "../assets/images/diamond.png";
import ring from "../assets/images/ring.png";
import theOneRing from "../assets/images/the one ring.png";
import earthquake from "../assets/images/earthquake.png";
import mountain from "../assets/images/mountain.png";
import yeti from "../assets/images/yeti.png";
import space from "../assets/images/space.png";
import tardis from "../assets/images/tardis.png";
import brick from "../assets/images/brick.png";
import wall from "../assets/images/wall.png";
import house from "../assets/images/house.png";
import hospital from "../assets/images/hospital.png";
import doctor from "../assets/images/doctor.png";
import theDoctor from "../assets/images/thedoctor.png";
import electric from "../assets/images/electric.png";
import sun from "../assets/images/sun.png";
import paper from "../assets/images/paper.png";
import snow from "../assets/images/snow.png";
import fruit from "../assets/images/fruit.png";
import sugar from "../assets/images/sugar.png";
import hay from "../assets/images/hay.png";
import farmer from "../assets/images/farmer.png";
import horse from "../assets/images/horse.png";
import fish from "../assets/images/fish.png";
import steel from "../assets/images/steel.png";
import wheel from "../assets/images/wheel.png";
import lightBulb from "../assets/images/light bulb.png";
import light from "../assets/images/light.png";
import car from "../assets/images/car.png";
import cactus from "../assets/images/cactus.png";
import desert from "../assets/images/desert.png";
import airplane from "../assets/images/airplane.png";
import cotton from "../assets/images/cotton.png";
import thread from "../assets/images/thread.png";
import fabric from "../assets/images/fabric.png";
import flower from "../assets/images/flower.png";
import rainbow from "../assets/images/rainbow.png";
import river from "../assets/images/river.png";
import field from "../assets/images/field.png";
import wheat from "../assets/images/wheat.png";
import flour from "../assets/images/flour.png";
import dough from "../assets/images/dough.png";
import cheese from "../assets/images/cheese.png";
import glasses from "../assets/images/glasses.png";
import bread from "../assets/images/bread.png";
import sunflower from "../assets/images/sunflower.png";
import oxygen from "../assets/images/oxygen.png";
import oil from "../assets/images/oil.png";
import carbonDioxide from "../assets/images/carbon dioxide.png";
import juice from "../assets/images/juice.png";
import pig from "../assets/images/pig.png";
import nerd from "../assets/images/nerd.png";
import computer from "../assets/images/computer.png";
import wire from "../assets/images/wire.png";
import meat from "../assets/images/meat.png";
import bullet from "../assets/images/bullet.png";
import gun from "../assets/images/gun.png";
import corpse from "../assets/images/corpse.png";
import sword from "../assets/images/sword.png";
import scissors from "../assets/images/scissors.png";
import sheep from "../assets/images/sheep.png";
import wool from "../assets/images/wool.png";
import christmasTree from "../assets/images/christmas tree.png";
import santa from "../assets/images/santa.png";
import lizard from "../assets/images/lizard.png";
import dinosaur from "../assets/images/dinosaur.png";
import armor from "../assets/images/armor.png";
import knight from "../assets/images/knight.png";
import castle from "../assets/images/castle.png";
import boat from "../assets/images/boat.png";
import clay from "../assets/images/clay.png";
import boiler from "../assets/images/boiler.png";
import steamEngine from "../assets/images/steam engine.png";
import gold from "../assets/images/gold.png";
import explosion from "../assets/images/explosion.png";
import beach from "../assets/images/beach.png";
import mouse from "../assets/images/mouse.png";
import squirrel from "../assets/images/squirrel.png";
import fireplace from "../assets/images/fireplace.png";
import pencil from "../assets/images/pencil.png";
import money from "../assets/images/money.png";
import sailor from "../assets/images/sailor.png";
import pirate from "../assets/images/pirate.png";
import goat from "../assets/images/goat.png";
import mountainGoat from "../assets/images/mountain goat.png";
import pottery from "../assets/images/pottery.png";
import clock from "../assets/images/clock.png";
import blood from "../assets/images/blood.png";
import rope from "../assets/images/rope.png";
import antarctica from "../assets/images/antarctica.png";
import alcohol from "../assets/images/alcohol.png";
import broom from "../assets/images/broom.png";
import witch from "../assets/images/witch.png";
import scythe from "../assets/images/scythe.png";
import wine from "../assets/images/wine.png";
import windmil from "../assets/images/windmil.png";
import snowball from "../assets/images/snowball.png";
import snowman from "../assets/images/snowman.png";
import vegetable from "../assets/images/vegetable.png";
import dam from "../assets/images/dam.png";
import beaver from "../assets/images/beaver.png";
import letter from "../assets/images/letter.png";
import engineer from "../assets/images/engineer.png";
import butterfly from "../assets/images/butterfly.png";
import bee from "../assets/images/bee.png";
import skeleton from "../assets/images/skeleton.png";
import bone from "../assets/images/bone.png";
import unicorn from "../assets/images/unicorn.png";
import chicken from "../assets/images/chicken.png";
import sailboat from "../assets/images/sailboat.png";
import cookie from "../assets/images/cookie.png";
import robot from "../assets/images/robot.png";
import wizard from "../assets/images/wizard.png";
import planet from "../assets/images/planet.png";
import skyscraper from "../assets/images/skyscraper.png";
import city from "../assets/images/city.png";
import lightsaber from "../assets/images/lightsaber.png";
import pirateship from "../assets/images/pirateship.png";
import fog from "../assets/images/fog.png";
import storm from "../assets/images/storm.png";
import bicycle from "../assets/images/bicycle.png";
import leaf from "../assets/images/leaf.png";
import palm from "../assets/images/ppalm.png";
import coconut from "../assets/images/coconut.png";
import leather from "../assets/images/leather.png";
import pumpkin from "../assets/images/pumpkin.png";
import statue from "../assets/images/statue.png";
import puddle from "../assets/images/puddle.png";
import carrot from "../assets/images/carrot.png";
import bacon from "../assets/images/bacon.png";
import sandwich from "../assets/images/sandwich.png";
import day from "../assets/images/day.png";
import vampire from "../assets/images/vampire.png";
import excalibur from "../assets/images/excalibur.png";
import monarch from "../assets/images/monarch.png";

export const dataInit = [
  {
    id: 0,
    name: "air",
    image: air,
  },
  {
    id: 1,
    name: "earth",
    image: earth,
  },
  {
    id: 2,
    name: "fire",
    image: fire,
  },
  {
    id: 3,
    name: "water",
    image: water,
  },
  ,
  {
    id: 4,
    name: "energy",
    image: energy,
  },
  {
    id: 5,
    name: "rain",
    image: rain,
  },
  {
    id: 6,
    name: "mud",
    image: mud,
  },
  {
    id: 7,
    name: "plant",
    image: plant,
  },
  {
    id: 8,
    name: "swamp",
    image: swamp,
  },
  {
    id: 9,
    name: "life",
    image: life,
  },
  {
    id: 10,
    name: "bacterial",
    image: bacterial,
  },
  {
    id: 11,
    name: "bird",
    image: bird,
  },
  {
    id: 12,
    name: "egg",
    image: egg,
  },
  {
    id: 13,
    name: "omelette",
    image: omelette,
  },
  {
    id: 14,
    name: "hard roe",
    image: hardroe,
  },
  {
    id: 15,
    name: "algae",
    image: algae,
  },
  {
    id: 16,
    name: "duck",
    image: duck,
  },
  {
    id: 17,
    name: "duckling",
    image: duckling,
  },
  {
    id: 18,
    name: "dust",
    image: dust,
  },
  {
    id: 19,
    name: "flood",
    image: flood,
  },
  {
    id: 20,
    name: "grass",
    image: grass,
  },
  {
    id: 21,
    name: "garden",
    image: garden,
  },
  {
    id: 22,
    name: "gunpowder",
    image: gunpowder,
  },
  {
    id: 23,
    name: "human",
    image: human,
  },
  {
    id: 24,
    name: "love",
    image: love,
  },
  {
    id: 25,
    name: "obsidian",
    image: obsidian,
  },
  {
    id: 26,
    name: "lava",
    image: lava,
  },
  {
    id: 27,
    name: "ostrich",
    image: ostrich,
  },
  {
    id: 28,
    name: "phoenix",
    image: phoenix,
  },
  {
    id: 29,
    name: "plankton",
    image: plankton,
  },
  {
    id: 30,
    name: "pond",
    image: pond,
  },
  {
    id: 31,
    name: "pressure",
    image: pressure,
  },
  {
    id: 32,
    name: "rose",
    image: rose,
  },
  {
    id: 33,
    name: "sea",
    image: sea,
  },
  {
    id: 34,
    name: "seagull",
    image: seagull,
  },
  {
    id: 35,
    name: "seaweek",
    image: seaweek,
  },
  {
    id: 36,
    name: "angel",
    image: angel,
  },
  {
    id: 37,
    name: "allergy",
    image: allergy,
  },
  {
    id: 38,
    name: "dew",
    image: dew,
  },
  {
    id: 39,
    name: "cold",
    image: cold,
  },
  {
    id: 40,
    name: "sickness",
    image: sickness,
  },
  {
    id: 41,
    name: "ocean",
    image: ocean,
  },
  {
    id: 42,
    name: "perfume",
    image: perfume,
  },
  {
    id: 43,
    name: "steam",
    image: steam,
  },
  {
    id: 44,
    name: "stone",
    image: stone,
  },
  {
    id: 45,
    name: "tobacco",
    image: tobacco,
  },
  {
    id: 46,
    name: "volcano",
    image: volcano,
  },
  {
    id: 47,
    name: "cloud",
    image: cloud,
  },
  {
    id: 48,
    name: "sky",
    image: sky,
  },
  {
    id: 49,
    name: "moon",
    image: moon,
  },
  {
    id: 50,
    name: "astronaut",
    image: astronaut,
  },
  {
    id: 51,
    name: "ice",
    image: ice,
  },
  {
    id: 52,
    name: "sand",
    image: sand,
  },
  {
    id: 53,
    name: "glass",
    image: glass,
  },
  {
    id: 54,
    name: "hourglass",
    image: hourglass,
  },
  {
    id: 55,
    name: "time",
    image: time,
  },
  {
    id: 56,
    name: "tree",
    image: tree,
  },
  {
    id: 57,
    name: "forest",
    image: forest,
  },
  {
    id: 58,
    name: "wild animal",
    image: wildAnimal,
  },
  {
    id: 59,
    name: "dog",
    image: dog,
  },
  {
    id: 60,
    name: "livestock",
    image: livestock,
  },
  {
    id: 61,
    name: "cow",
    image: cow,
  },
  {
    id: 62,
    name: "milk",
    image: milk,
  },
  {
    id: 63,
    name: "minotaur",
    image: minotaur,
  },
  {
    id: 64,
    name: "ice cream",
    image: iceCream,
  },
  {
    id: 65,
    name: "astronaut ice cream",
    image: astronautIceCream,
  },
  {
    id: 66,
    name: "cat",
    image: cat,
  },
  {
    id: 67,
    name: "wind",
    image: wind,
  },
  {
    id: 68,
    name: "wave",
    image: wave,
  },
  {
    id: 69,
    name: "sound",
    image: sound,
  },
  {
    id: 70,
    name: "music",
    image: music,
  },
  {
    id: 71,
    name: "keyboard cat",
    image: keyboardCat,
  },
  {
    id: 72,
    name: "metal",
    image: metal,
  },
  {
    id: 73,
    name: "tool",
    image: tool,
  },
  {
    id: 74,
    name: "wood",
    image: wood,
  },
  {
    id: 75,
    name: "charcoal",
    image: charcoal,
  },
  {
    id: 76,
    name: "smoke",
    image: smoke,
  },
  {
    id: 77,
    name: "campfire",
    image: campfire,
  },
  {
    id: 78,
    name: "lake",
    image: lake,
  },
  {
    id: 79,
    name: "story",
    image: story,
  },
  {
    id: 80,
    name: "nessie",
    image: nessie,
  },
  {
    id: 81,
    name: "night",
    image: night,
  },
  {
    id: 82,
    name: "blade",
    image: blade,
  },
  {
    id: 83,
    name: "star",
    image: star,
  },
  {
    id: 84,
    name: "shuriken",
    image: shuriken,
  },
  {
    id: 85,
    name: "ninja",
    image: ninja,
  },
  {
    id: 86,
    name: "turtle",
    image: turtle,
  },
  {
    id: 87,
    name: "ninja turtle",
    image: ninjaTurtle,
  },
  {
    id: 88,
    name: "coal",
    image: coal,
  },
  {
    id: 89,
    name: "diamond",
    image: diamond,
  },
  {
    id: 90,
    name: "ring",
    image: ring,
  },
  {
    id: 91,
    name: "the one ring",
    image: theOneRing,
  },
  {
    id: 92,
    name: "earthquake",
    image: earthquake,
  },
  {
    id: 93,
    name: "mountain",
    image: mountain,
  },
  {
    id: 94,
    name: "yeti",
    image: yeti,
  },
  {
    id: 95,
    name: "space",
    image: space,
  },
  {
    id: 96,
    name: "tardis",
    image: tardis,
  },
  {
    id: 97,
    name: "brick",
    image: brick,
  },
  {
    id: 98,
    name: "wall",
    image: wall,
  },
  {
    id: 99,
    name: "house",
    image: house,
  },
  {
    id: 100,
    name: "hospital",
    image: hospital,
  },
  {
    id: 101,
    name: "doctor",
    image: doctor,
  },
  {
    id: 102,
    name: "the doctor",
    image: theDoctor,
  },
  {
    id: 103,
    name: "electric",
    image: electric,
  },
  {
    id: 104,
    name: "sun",
    image: sun,
  },
  {
    id: 105,
    name: "paper",
    image: paper,
  },
  {
    id: 106,
    name: "snow",
    image: snow,
  },
  {
    id: 107,
    name: "fruit",
    image: fruit,
  },
  {
    id: 108,
    name: "sugar",
    image: sugar,
  },
  {
    id: 109,
    name: "hay",
    image: hay,
  },
  {
    id: 110,
    name: "farmer",
    image: farmer,
  },
  {
    id: 111,
    name: "horse",
    image: horse,
  },
  {
    id: 112,
    name: "fish",
    image: fish,
  },
  {
    id: 113,
    name: "steel",
    image: steel,
  },
  {
    id: 114,
    name: "wheel",
    image: wheel,
  },
  {
    id: 115,
    name: "light bulb",
    image: lightBulb,
  },
  {
    id: 116,
    name: "light",
    image: light,
  },
  {
    id: 117,
    name: "car",
    image: car,
  },
  {
    id: 118,
    name: "cactus",
    image: cactus,
  },
  {
    id: 119,
    name: "desert",
    image: desert,
  },
  {
    id: 120,
    name: "airplane",
    image: airplane,
  },
  {
    id: 121,
    name: "cotton",
    image: cotton,
  },
  {
    id: 122,
    name: "thread",
    image: thread,
  },
  {
    id: 123,
    name: "fabric",
    image: fabric,
  },
  {
    id: 124,
    name: "flower",
    image: flower,
  },
  {
    id: 125,
    name: "rainbow",
    image: rainbow,
  },
  {
    id: 126,
    name: "river",
    image: river,
  },
  {
    id: 127,
    name: "field",
    image: field,
  },
  {
    id: 128,
    name: "wheat",
    image: wheat,
  },
  {
    id: 129,
    name: "flour",
    image: flour,
  },
  {
    id: 130,
    name: "dough",
    image: dough,
  },
  {
    id: 131,
    name: "cheese",
    image: cheese,
  },
  {
    id: 132,
    name: "glasses",
    image: glasses,
  },
  {
    id: 133,
    name: "bread",
    image: bread,
  },
  {
    id: 134,
    name: "sunflower",
    image: sunflower,
  },
  {
    id: 135,
    name: "oxygen",
    image: oxygen,
  },
  {
    id: 136,
    name: "oil",
    image: oil,
  },
  {
    id: 137,
    name: "carbon dioxide",
    image: carbonDioxide,
  },
  {
    id: 138,
    name: "juice",
    image: juice,
  },
  {
    id: 139,
    name: "pig",
    image: pig,
  },
  {
    id: 140,
    name: "nerd",
    image: nerd,
  },
  {
    id: 141,
    name: "computer",
    image: computer,
  },
  {
    id: 142,
    name: "wire",
    image: wire,
  },
  {
    id: 143,
    name: "meat",
    image: meat,
  },
  {
    id: 144,
    name: "bullet",
    image: bullet,
  },
  {
    id: 145,
    name: "gun",
    image: gun,
  },
  {
    id: 146,
    name: "corpse",
    image: corpse,
  },
  {
    id: 147,
    name: "sword",
    image: sword,
  },
  {
    id: 148,
    name: "scissors",
    image: scissors,
  },
  {
    id: 149,
    name: "sheep",
    image: sheep,
  },
  {
    id: 150,
    name: "wool",
    image: wool,
  },
  {
    id: 151,
    name: "christmas tree",
    image: christmasTree,
  },
  {
    id: 152,
    name: "santa",
    image: santa,
  },
  {
    id: 153,
    name: "lizard",
    image: lizard,
  },
  {
    id: 154,
    name: "dinosaur",
    image: dinosaur,
  },
  {
    id: 155,
    name: "armor",
    image: armor,
  },
  {
    id: 156,
    name: "knight",
    image: knight,
  },
  {
    id: 157,
    name: "castle",
    image: castle,
  },
  {
    id: 158,
    name: "boat",
    image: boat,
  },
  {
    id: 159,
    name: "clay",
    image: clay,
  },
  {
    id: 160,
    name: "boiler",
    image: boiler,
  },
  {
    id: 161,
    name: "steam engine",
    image: steamEngine,
  },
  {
    id: 162,
    name: "gold",
    image: gold,
  },
  {
    id: 163,
    name: "explosion",
    image: explosion,
  },
  {
    id: 164,
    name: "beach",
    image: beach,
  },
  {
    id: 165,
    name: "mouse",
    image: mouse,
  },
  {
    id: 166,
    name: "squirrel",
    image: squirrel,
  },
  {
    id: 167,
    name: "fireplace",
    image: fireplace,
  },
  {
    id: 168,
    name: "pencil",
    image: pencil,
  },
  {
    id: 169,
    name: "money",
    image: money,
  },
  {
    id: 170,
    name: "sailor",
    image: sailor,
  },
  {
    id: 171,
    name: "pirate",
    image: pirate,
  },
  {
    id: 172,
    name: "goat",
    image: goat,
  },
  {
    id: 173,
    name: "mountain goat",
    image: mountainGoat,
  },
  {
    id: 174,
    name: "pottery",
    image: pottery,
  },
  {
    id: 175,
    name: "clock",
    image: clock,
  },
  {
    id: 176,
    name: "blood",
    image: blood,
  },
  {
    id: 177,
    name: "rope",
    image: rope,
  },
  {
    id: 178,
    name: "antarctica",
    image: antarctica,
  },
  {
    id: 179,
    name: "alcohol",
    image: alcohol,
  },
  {
    id: 180,
    name: "broom",
    image: broom,
  },
  {
    id: 181,
    name: "witch",
    image: witch,
  },
  {
    id: 182,
    name: "scythe",
    image: scythe,
  },
  {
    id: 183,
    name: "wine",
    image: wine,
  },
  {
    id: 184,
    name: "windmil",
    image: windmil,
  },
  {
    id: 185,
    name: "snowball",
    image: snowball,
  },
  {
    id: 186,
    name: "snowman",
    image: snowman,
  },
  {
    id: 187,
    name: "vegetable",
    image: vegetable,
  },
  {
    id: 188,
    name: "dam",
    image: dam,
  },
  {
    id: 189,
    name: "beaver",
    image: beaver,
  },
  {
    id: 190,
    name: "letter",
    image: letter,
  },
  {
    id: 191,
    name: "engineer",
    image: engineer,
  },
  {
    id: 192,
    name: "butterfly",
    image: butterfly,
  },
  {
    id: 193,
    name: "bee",
    image: bee,
  },
  {
    id: 194,
    name: "skeleton",
    image: skeleton,
  },
  {
    id: 195,
    name: "bone",
    image: bone,
  },
  {
    id: 196,
    name: "unicorn",
    image: unicorn,
  },
  {
    id: 197,
    name: "chicken",
    image: chicken,
  },
  {
    id: 198,
    name: "sailboat",
    image: sailboat,
  },
  {
    id: 199,
    name: "cookie",
    image: cookie,
  },
  {
    id: 200,
    name: "robot",
    image: robot,
  },
  {
    id: 201,
    name: "wizard",
    image: wizard,
  },
  {
    id: 202,
    name: "planet",
    image: planet,
  },
  {
    id: 203,
    name: "skyscraper",
    image: skyscraper,
  },
  {
    id: 204,
    name: "city",
    image: city,
  },
  {
    id: 205,
    name: "lightsaber",
    image: lightsaber,
  },
  {
    id: 206,
    name: "pirate ship",
    image: pirateship,
  },
  {
    id: 207,
    name: "fog",
    image: fog,
  },
  {
    id: 208,
    name: "storm",
    image: storm,
  },
  {
    id: 209,
    name: "bicycle",
    image: bicycle,
  },
  {
    id: 210,
    name: "leaf",
    image: leaf,
  },
  {
    id: 211,
    name: "palm",
    image: palm,
  },
  {
    id: 212,
    name: "coconut",
    image: coconut,
  },
  {
    id: 213,
    name: "leather",
    image: leather,
  },
  {
    id: 214,
    name: "pumpkin",
    image: pumpkin,
  },
  {
    id: 215,
    name: "statue",
    image: statue,
  },
  {
    id: 216,
    name: "puddle",
    image: puddle,
  },
  {
    id: 217,
    name: "carrot",
    image: carrot,
  },
  {
    id: 218,
    name: "bacon",
    image: bacon,
  },
  {
    id: 219,
    name: "sandwich",
    image: sandwich,
  },
  {
    id: 220,
    name: "day",
    image: day,
  },
  {
    id: 221,
    name: "vampire",
    image: vampire,
  },
  {
    id: 222,
    name: "excalibur",
    image: excalibur,
  },
  {
    id: 223,
    name: "monarch",
    image: monarch,
  },
];