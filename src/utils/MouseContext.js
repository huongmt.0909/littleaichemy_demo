import { createContext, useState } from "react";
import { useContext } from "react";
import { AppContext } from "./AppContext";
import { v4 as uuidv4 } from "uuid";

const MouseContext = createContext();

function MouseProvider({ children }) {
  const valueContent = useContext(AppContext);

  const [isSelectItemInSideBar, setIsSelectItemInSideBar] = useState(false);
  const [itemSelect, setItemSelect] = useState({});
  const [position, setPosition] = useState({});
  const [isMouseMove, setIsMouseMove] = useState(false);
  const [itemFocus, setItemFocus] = useState("");

  const handleMouseDown = (item, value) => {
    setItemFocus(value);
    setIsSelectItemInSideBar(true);
    setItemSelect(item);
    setIsMouseMove(false);
    valueContent.setMoveItemPosition(true);
  };

  const handleMouseEnter = (ev) => {
    if (itemFocus.type === "sidebar") {
      setPosition({
        left: ev.clientX,
        top: ev.clientY,
      });
    }
  };

  const handleMouseUp = () => {
    //add newItem
    let newData = {
      idItem: uuidv4(),
      item: itemSelect,
      position: position,
    };
    if (
      Object.values(itemSelect).length > 0 &&
      position.left < window.screen.width - 282
      && isMouseMove === true
    ) {
      valueContent.setDataContent([...valueContent.dataContent, newData]);
    }

    //delete Item
    if (
      itemFocus.type === "content" &&
      Object.values(itemSelect).length > 0 &&
      position.left > window.screen.width - 282
    ) {
      const newData = valueContent.dataContent;
      valueContent.setDataContent(
        newData.filter((item, index) => index !== itemFocus.ix)
      );
    }

    //change position Item
    if (itemFocus.type === "content") {
      valueContent.dataContent.map((data, index) => {
        if (index === itemFocus.ix) {
          data.position = position;
        }
      });
    }

    //check item when collide
    valueContent.checkRecipes();

    setIsSelectItemInSideBar(false);
    setItemSelect({});
    setPosition({});
    setIsMouseMove(false);
  };

  const handleOnMouseMove = (ev) => {
    if (isSelectItemInSideBar) {
      setIsMouseMove(true);

      setPosition({
        left: ev.clientX,
        top: ev.clientY,
      });

      //change position and delete item in old position
      if (itemFocus.type === "content") {
        const newData = valueContent.dataContent.filter(
          (item, index) => index !== itemFocus.ix
        );
        valueContent.setDataContent(newData);
        setItemFocus({});
      }
      valueContent.setMoveItemPosition(false);

      checkCollide();
    }
  };

  function checkCollide() {
    if (valueContent.dataContent.length >= 1) {
      const newData = valueContent.dataContent.filter(
        (item, index) =>
          item.position.left - 32 <= position.left &&
          position.left <= item.position.left + 32 &&
          item.position.top - 32 <= position.top &&
          position.top <= item.position.top + 32
      );
      let formData = {
        item: itemSelect,
        position: position,
      };
      valueContent.setItemIsSelect(formData);
      valueContent.setItemDuplicate(
        newData[newData.length - 1] != undefined
          ? newData[newData.length - 1]
          : []
      );
    }
  }

  const value = {
    isSelectItemInSideBar,
    handleMouseDown,
    handleMouseUp,
    itemSelect,
    handleOnMouseMove,
    position,
    handleMouseEnter,
    isMouseMove,
    itemFocus,
    setItemFocus,
  };
  return (
    <MouseContext.Provider value={value}>{children}</MouseContext.Provider>
  );
}

export { MouseContext, MouseProvider };
