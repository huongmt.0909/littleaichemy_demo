import { createContext, useEffect, useState } from "react";
import { dataInit } from "./dataInit";
import filterList from "./dataFilter";
import dataRecipes from "./dataRecipes";
import { v4 as uuidv4 } from "uuid";

const AppContext = createContext();

function AppProvider({ children }) {
  const [dataContent, setDataContent] = useState(
    JSON.parse(localStorage.getItem("listItemContent"))
      ? JSON.parse(localStorage.getItem("listItemContent"))
      : []
  );
  const [itemDuplicate, setItemDuplicate] = useState([]);
  const [itemIsSelect, setItemIsSelect] = useState({});
  const [isMoveItemPosition, setMoveItemPosition] = useState(true);
  let dataStart = dataInit;
  const [dataCreate, setDataCreate] = useState(
    JSON.parse(localStorage.getItem("listItemSideBar"))
      ? JSON.parse(localStorage.getItem("listItemSideBar"))
      : dataStart.filter((item) => item.id < 4)
  );

  useEffect(() => {
    setDataCreate(dataCreate.sort(dynamicSort("name")));

    localStorage.setItem("listItemSideBar", JSON.stringify(dataCreate));
  }, [dataCreate]);

  function dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function (a, b) {
      var result =
        a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
      return result * sortOrder;
    };
  }

  useEffect(() => {
    localStorage.setItem("listItemContent", JSON.stringify(dataContent));
  }, [dataContent]);

  function checkRecipes() {
    if (
      Object.values(itemDuplicate).length > 0 &&
      Object.values(itemIsSelect).length > 0
    ) {
      const listNewItem = dataRecipes.filter(
        (item) =>
          (item[0] === itemDuplicate.item.id &&
            item[1] === itemIsSelect.item.id) ||
          (item[1] === itemDuplicate.item.id &&
            item[0] === itemIsSelect.item.id)
      );
      if (listNewItem.length > 0) {
        let newDataContent = [];
        newDataContent = dataContent.filter(
          (item) => item.idItem !== itemDuplicate.idItem
        );
        listNewItem.map((item) => {
          const createNewItem = dataInit.filter((it) => it.id === item[2]);
          const indexOf = dataCreate.findIndex(
            (item) => item.id === createNewItem[0].id
          );
          if (indexOf == -1) {
            setDataCreate([...dataCreate, createNewItem[0]]);
          }
          const formData = {
            idItemRemove: itemDuplicate.idItem,
            newCreateItem: {
              idItem: uuidv4(),
              item: createNewItem[0],
              position: itemDuplicate.position,
            },
          };

          newDataContent.push(formData.newCreateItem);
        });
        setDataContent(newDataContent);
        setItemDuplicate([]);
        setItemIsSelect({});
        return true;
      }
    }
    return null;
  }

  const value = {
    filterList,
    dataCreate,
    dataContent,
    setDataContent,
    isMoveItemPosition,
    setMoveItemPosition,
    itemDuplicate,
    itemIsSelect,
    setItemDuplicate,
    setItemIsSelect,
    checkRecipes,
    dataInit
  };
  return (
    <AppContext.Provider value={value}>
      {children}
    </AppContext.Provider>
  );
}

export { AppContext, AppProvider };
