import { useContext, useState } from "react";
import { MouseContext } from "../../utils/MouseContext";
import { AppContext } from "../../utils/AppContext";

import Element from "../../components/Element";
import "./style.scss";
import SideBar from "../sidebars";
import clearImg from "../../assets/images/clear.png";
import fullScreen from "../../assets/images/fullscr.png";
import closeFullScreen from "../../assets/images/closefullscreen.png";

function Content() {
  const valueMouseContext = useContext(MouseContext);
  const valueContent = useContext(AppContext);
  const [isFullScreen, setFullScreen] = useState(false);

  const handleResetItemInContent = () => {
    valueContent.setDataContent([]);
  };

  const handleFullScreenClick = () => {
    const element = document.getElementById("element");
    if (element.requestFullscreen) element.requestFullscreen();
    else if (element.mozRequestFullScreen) element.mozRequestFullScreen();
    else if (element.webkitRequestFullscreen) element.webkitRequestFullscreen();
    else if (element.msRequestFullscreen) element.msRequestFullscreen();
    setFullScreen(true)
  };

  const handleCloseFullScreenClick = () => {
    if (document.exitFullscreen) document.exitFullscreen();
    else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
    else if (document.webkitExitFullscreen) document.webkitExitFullscreen();
    else if (document.msExitFullscreen) document.msExitFullscreen();
    setFullScreen(false)

  };
  const checkItemDuplicate = (idItemDuplicate, idItemInContent) => {
    if (valueMouseContext.isMouseMove) {
      if (valueContent.dataContent.length > 0 && valueContent.itemDuplicate) {
        if (idItemDuplicate === idItemInContent) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else return false;
  };

  return (
    <div
      className="content"
      onMouseUp={valueMouseContext.handleMouseUp}
      onMouseMove={valueMouseContext.handleOnMouseMove}
    >
      <div className="content_main">
        {valueMouseContext.isMouseMove &&
          Object.values(valueMouseContext.itemSelect).length > 0 && (
            <Element
              img={valueMouseContext.itemSelect.image}
              name={valueMouseContext.itemSelect.name}
              position={
                Object.values(valueMouseContext.itemSelect).length !== 0
                  ? valueMouseContext.position
                  : null
              }
              hidden={valueMouseContext.isMouseMove ? true : false}
              zIndex="101"
            />
          )}
        {valueContent.dataContent.map((element, index) => (
          <Element
            key={index}
            img={element.item.image}
            name={element.item.name}
            position={element.position}
            onMouseDown={() =>
              valueMouseContext.handleMouseDown(element.item, {
                type: "content",
                ix: index,
              })
            }
            onMouseEnter={(ev) => valueMouseContext.handleMouseEnter(ev)}
            opacity={
              checkItemDuplicate(
                valueContent.itemDuplicate.idItem,
                element.idItem
              )
                ? 0.5
                : 1
            }
          />
        ))}
      </div>
      <SideBar />
      <div className="total_item">
          <p>{valueContent.dataCreate.length} / {valueContent.dataInit.length}</p>
      </div>
      <div className="control_reset" onClick={handleResetItemInContent}>
        <button>
          <img src={clearImg} alt="photo" />
        </button>
      </div>
      {isFullScreen === false && (
        <div className="control_screen" onClick={handleFullScreenClick}>
          <button>
            <img src={fullScreen} alt="photo" />
          </button>
        </div>
      )}
      {isFullScreen && (
        <div className="control_screen" onClick={handleCloseFullScreenClick}>
          <button>
            <img src={closeFullScreen} alt="photo" />
          </button>
        </div>
      )}
    </div>
  );
}

export default Content;
