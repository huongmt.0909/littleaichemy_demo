import { useContext, useEffect } from "react";
import { MouseContext } from "../../utils/MouseContext";
import { AppContext } from "../../utils/AppContext";

import "./style.scss";

function SideBar() {
  const valueMouseContext = useContext(MouseContext);
  const valueAppContext = useContext(AppContext);

  const handleClickScroll = (item) => {
    let elementId = "";

    for (
      let index = 0;
      index < valueAppContext.dataCreate.length;
      index++
    ) {
      const element = valueAppContext.dataCreate[index];
      if (element.name.charAt(0).toUpperCase() == item.name) {
        elementId = element.name;
        break;
      }
    }
    if (elementId != "") {
      const element = document.getElementById(elementId);
      element.scrollIntoView({ behavior: "smooth" });
    }
  };
  return (
    <div className="side-bar">
      <div className="side-bar_filter">
        <ul>
          {valueAppContext.filterList.map((item) => (
            <li key={item.id} onClick={() => handleClickScroll(item)}>
              <p>{item.name}</p>
            </li>
          ))}
        </ul>
      </div>
      <div className="side-bar_list">
        <ul>
          {valueAppContext.dataCreate.map((item, index) => (
            <li key={item.id} id={item.name}>
              <div>
                <img src={item.image} alt="photo" />
                <div
                  className="overlay"
                  onMouseDown={() =>
                    valueMouseContext.handleMouseDown(item, {
                      type: "sidebar",
                      ix: index,
                    })
                  }
                  onMouseEnter={(ev) => valueMouseContext.handleMouseEnter(ev)}
                >
                  {" "}
                </div>
              </div>
              <p>{item.name}</p>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default SideBar;
