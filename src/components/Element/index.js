import "./style.scss";
function Element({ img, name, position, onMouseDown, onMouseEnter, zIndex, opacity }) {
  return (
    <div
      className="element"
      style={
        position != null
          ? { left: position.left - 32, top: position.top - 32, zIndex:zIndex, opacity: opacity }
          : { left: 1741, top: 85, display: "none" }
      }
    >
      <div>
        <img src={img} alt="photo" />
        <div className="element_ovelay" onMouseDown={onMouseDown} onMouseEnter={onMouseEnter}></div>
      </div>
      <p>{name}</p>
    </div>
  );
}

export default Element;
